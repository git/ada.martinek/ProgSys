#!/bin/bash

n=10
rdv=$(semC $n)
for i in $n; do
	./rdv-client.sh $rdv $i &
done
wait
semD $rdv
echo "Suppression du sémaphore de rdv"
