#!/bin/bash

attendreP2=$(semC 0)
attendreP1=$(semC 0)
for i in 1; do
	./rdv-client.sh $attendreP1 $attendreP2 1 &
	./rdv-client.sh $attendreP2 $attendreP1 2 &
done
wait
semD $attendreP1
echo "Suppression du sémaphore de rdv (id=$attendreP1)"
semD $attendreP2
echo "Suppression du sémaphore de rdv (id=$attendreP2)"
