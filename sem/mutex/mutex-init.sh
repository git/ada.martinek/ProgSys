#!/bin/sh
 
mutex=$(semC)
echo "Création du sémaphore d'exclusion mutuelle (id=$mutex)"
for i in 1 2 3; do
   ./mutex-client.sh $mutex $i &
done
wait
semD $mutex
echo "Suppression du sémaphore d'exclusion mutuelle (id=$mutex)"
