#!/bin/sh
 
echo "[`date +%M:%S`](client n°$2) Je demande la ressource."
semP $1
echo "[`date +%M:%S`](client n°$2) J'utilise la ressource."
sleep 2
echo "[`date +%M:%S`](client n°$2) Je libère la ressource."
semV $1
