#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <ctype.h>
#define REQUEST_MAX 1024

void    usage()
{
    fprintf(stderr,"usage : serveur port\n");
    exit(1);
}

int main(int argc, char **argv)
{
    int         ret;
    int         sock;
    int         s;
	socklen_t   len_src_addr;
    char        request[REQUEST_MAX];

    struct addrinfo hints, *result;
    struct sockaddr_storage src_addr;

    if(argc!=2)
    {
        fprintf(stderr,"Erreur : Nb args !\n");
        usage();
    }
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_INET6;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;
    ret = getaddrinfo(NULL, argv[1], &hints, &result);
    if(ret != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
        exit(EXIT_FAILURE);
    }
    if((s=socket(result->ai_family, result->ai_socktype, result->ai_protocol))==-1)
    {
        perror("socket");
        exit(1);
    }
    if(bind(s, result->ai_addr, result->ai_addrlen) == -1)
    {
        perror("bind");
        exit(1);
    }
    freeaddrinfo(result);
    if(listen(s, 5))
    {
        perror("listen");
        exit(1);
    }
    while(1)
    {
        puts("En attente de connexion...");
        len_src_addr=sizeof src_addr;
        if((sock=accept(s, (struct sockaddr *)&src_addr, &len_src_addr))==-1)
        {
            perror("accept");
            exit(1);
        }
        puts("Connexion acceptée !");
        while((ret=recv(sock, request, REQUEST_MAX,0))>0)
        {
            request[ret]=0;
            {
                int i=0;

                while(request[i]) {
                    request[i]=toupper(request[i]);
                    ++i;
                }
            }
            if(send(sock, request, strlen(request)+1,0) != strlen(request)+1)
            {
                perror("send");
                exit(1);
            }
        }
        if(close(sock)==-1)
        {
            perror("close");
            exit(1);
        }
        fprintf(stderr, "Fin de connexion !\n");
        if(ret==-1)
        {
            perror("recv");
        }
    }
}