#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#define LINE_MAX 1024

void    usage()
{
    fprintf(stderr,"usage : client hostname port\n");
    exit(1);
}

int main(int argc, char **argv)
{
    int     s;
    int     ret;
    char    msg[LINE_MAX];
    char    response[LINE_MAX];

    struct addrinfo hints, *result;

    if(argc != 3)
    {
        fprintf(stderr,"Erreur : Nb args !\n");
        usage();
    }
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_flags = 0;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;
    ret = getaddrinfo(argv[1], argv[2], &hints, &result);
    if(ret != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
        exit(EXIT_FAILURE);
    }
    if((s = socket(result->ai_family, result->ai_socktype, result->ai_protocol)) == -1)
    {
        perror("socket");
        exit(1);
    }
    if(connect(s, result->ai_addr, result->ai_addrlen))
    {
        perror("connect");
        exit(1);
    }
    freeaddrinfo(result);
    msg[strlen(msg) - 1] = 0;
    if(send(s, msg, strlen(msg) + 1, 0) != strlen(msg) + 1)
    {
        perror("send");
        exit(1);
    }
    ret = recv(s, response, LINE_MAX, 0);

    if(ret == 0)
    {
        fprintf(stderr, "Déconnexion du serveur !\n");
        exit(1);
    }

    else if(ret == -1)
    {
        perror("recv");
        exit(1);
    }
    puts(response);
    if(close(s) == -1)
    {
        perror("close");
        exit(1);
    }
    return 0;
}