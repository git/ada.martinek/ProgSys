#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <limits.h>
#include <errno.h>

void	my_putnbr(int nb)
{
	char c;

	if (nb < 0)
	{
		if (nb == -2147483648)
		{
			write(1, "-", 1);
			write(1, "2", 1);
			my_putnbr(147483648);
			return ;
		}
		nb = nb * -1;
		write(1, "-", 1);
	}
	if (nb >= 0 && nb < 10)
	{
		c = nb + '0';
		write(1, &c, 1);
	}
	if (nb >= 10)
	{
		my_putnbr(nb / 10);
		my_putnbr(nb % 10);
	}
}

void	my_putnbrUnsignedLong(long nb)
{
	char c;

	if (nb >= 0 && nb < 10)
	{
		c = nb + '0';
		write(1, &c, 1);
	}
	if (nb >= 10)
	{
		my_putnbr(nb / 10);
		my_putnbr(nb % 10);
	}
}

int	my_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len] != '\0')
		len++;
	return (len);
}

char	*my_strcat(char *dest, char *src, int len)
{
	int	i;
	int	j;
	char	*final;
	
	i = 0;
	j = 0;
	final = (char*)malloc(len);
	while (dest[i] != '\0')
	{
		final[i] = dest[i];
		i++;
	}
	final[i] = '/';
	i++;
	while (src[j] != '\0')
	{
		final[i] = src[j];
		i++;
		j++;
	}
	final[i] = '\0';
	return (final);
}

void	showFileInfos(char *fileName)
{
	struct	stat fileStat;

	if(lstat(fileName, &fileStat) == -1)
	{
		write(1, "Foirage du statage\n", 20);
		exit(1);
	}
	write(1, fileName, my_strlen(fileName));
	write(1, " ", 1);	
	my_putnbr(fileStat.st_size);
	write(1, " ", 1);	
	my_putnbrUnsignedLong(fileStat.st_mtim.tv_sec);
	write(1, "\n", 2);
}

int	my_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i])
	{
		if (s1[i] != s2[i])
			return (s1[i] - s2[i]);
		i++;
	}
	return (s1[i] - s2[i]);
}

void	listDirContent(char *leDossier)
{
	DIR	*dir;
	struct	dirent *lesEntrees;
	char	*fileAbsolPath;
	size_t	path_length;

	dir = opendir(leDossier);
	if(!dir)
	{
		if(errno == ENOTDIR) //C'est quand meme pas mal errno, je decouvre
		{
			write(1, "t'es bourré, on veut le nom d'un dossier et pas autre chose\n", 61);
			exit(1);
		}
		write(1, "Probleme ouverture dossier, droit d'acces ?\n", 45);
		return;
	}
	path_length = my_strlen(leDossier) + NAME_MAX + 2;
	fileAbsolPath = (char*)malloc(path_length);
	while((lesEntrees = readdir(dir)))
	{
		if(lesEntrees->d_type == DT_DIR && my_strcmp(lesEntrees->d_name,".") && my_strcmp(lesEntrees->d_name,".."))
		{
			fileAbsolPath = my_strcat((char *)leDossier, lesEntrees->d_name, path_length);
			showFileInfos(fileAbsolPath);
			listDirContent(fileAbsolPath);
		}
		else
		{

			fileAbsolPath = my_strcat((char *)leDossier, lesEntrees->d_name, path_length);
			showFileInfos(fileAbsolPath);
		}
	}
	free(fileAbsolPath);
	closedir(dir);
}

int	main(int argc, char **argv)
{
	if(argc != 2)
	{
		write(1, "t'es bourré, on veut le nom d'un dossier qu'on t'dis\n", 54);
		exit(1);
	}
	listDirContent(argv[1]);
	return 0;
}
