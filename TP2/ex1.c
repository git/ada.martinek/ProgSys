#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>

void	my_putnbr(int nb)
{
	char c;

	if (nb < 0)
	{
		if (nb == -2147483648)
		{
			write(1, "-", 1);
			write(1, "2", 1);
			my_putnbr(147483648);
			return ;
		}
		nb = nb * -1;
		write(1, "-", 1);
	}
	if (nb >= 0 && nb < 10)
	{
		c = nb + '0';
		write(1, &c, 1);
	}
	if (nb >= 10)
	{
		my_putnbr(nb / 10);
		my_putnbr(nb % 10);
	}
}

void	my_putnbrUnsignedLong(long nb)
{
	char c;

	if (nb >= 0 && nb < 10)
	{
		c = nb + '0';
		write(1, &c, 1);
	}
	if (nb >= 10)
	{
		my_putnbr(nb / 10);
		my_putnbr(nb % 10);
	}
}

int	my_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len] != '\0')
		len++;
	return (len);
}

void	showFileInfos(char *fileName)
{
	struct	stat fileStat;

	if(lstat(fileName, &fileStat) == -1)
	{
		write(1, "Foirage du statage\n", 20);
		exit(1);
	}
	write(1, fileName, my_strlen(fileName));
	write(1, " ", 1);	
	my_putnbr(fileStat.st_size);
	write(1, " ", 1);	
	my_putnbrUnsignedLong(fileStat.st_mtim.tv_sec);
	write(1, "\n", 2);
}

int	main(int argc, char **argv)
{
	if(argc != 2)
	{
		write(1, "T'es bourré, on veut le nom d'un fichier qu'on t'dis\n", 54);
		exit(1);
	}
	showFileInfos(argv[1]);
	return 0;
}
