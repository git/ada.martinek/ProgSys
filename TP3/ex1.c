#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_MSG_LEN 256
#define MAX_PROC 10

int	my_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len] != '\0')
		len++;
	return (len);
}

int	main(void)
{
	const int	N = MAX_PROC;
	int		fd[MAX_PROC][2];
	pid_t		pid[MAX_PROC];
	char		buf[MAX_MSG_LEN];
	int		dest;
	char		msg[MAX_MSG_LEN];
	int		i;

	i = 0;
	while(i < N)
    	{
		if(pipe(fd[i]) == -1)
		{
			perror("foirage du pipage");
			exit(EXIT_FAILURE);
		}
		pid[i] = fork();
		if (pid[i] == -1)
		{
			perror("foirage du forkage");
			exit(EXIT_FAILURE);
		}
        	if(pid[i] == 0)
		{
			close(fd[i][1]);
			while(read(fd[i][0], buf, MAX_MSG_LEN) > 0)
			{
				printf("Processus %d a reçu: %s\n", i, buf);
			}
			close(fd[i][0]);
			exit(EXIT_SUCCESS);
		}
		i++;
	}
	i = 0;
	while(i < N)
	{
		close(fd[i][0]);
		i++;
	}
	while(scanf("%d %[^\n]", &dest, msg) != EOF)
	{
		if(dest >= 0 && dest < N)
		{
			write(fd[dest][1], msg, my_strlen(msg) + 1);
		}
	}
	i = 0;
	while(i < N)
	{
		close(fd[i][1]);
		i++;
	}
	return 0;
}
