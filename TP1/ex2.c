#include <stdlib.h>
#include <unistd.h>

int main() {
    write(1, "Appel à la commande 'ps -f' via system :\n", 42);
    system("ps -f");
    write(1, "\nRecouvrement par la même commande 'ps -f' via execlp :\n", 57);
    execlp("ps", "ps", "-f", (char *)NULL);
    return 0;
}
