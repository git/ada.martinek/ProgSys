#include <unistd.h>
#include <stdlib.h>

int	my_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len] != '\0')
		len++;
	return (len);
}

void	my_putnbr(int nb)
{
	char c;

	if (nb < 0)
	{
		if (nb == -2147483648)
		{
			write(1, "-", 1);
			write(1, "2", 1);
			my_putnbr(147483648);
			return ;
		}
		nb = nb * -1;
		write(1, "-", 1);
	}
	if (nb >= 0 && nb < 10)
	{
		c = nb + '0';
		write(1, &c, 1);
	}
	if (nb >= 10)
	{
		my_putnbr(nb / 10);
		my_putnbr(nb % 10);
	}
}

int		main(int argc, char *argv[])
{
	int	i;
	int	j;
	char	*path = getenv("PATH");
	
	i = 0;
	j = 0;
	if(!path)
	{
		write(1, "PATH non recuperee, abort\n", 26);
		exit(1);
	}
	while (i < argc)
	{
		write(1, "arg[", 4);
		my_putnbr(i);
		write(1, "]=", 2);
		j = my_strlen(argv[i]);
		write(1, argv[i], j);
		write(1, "\n", 1);
		i++;
	}
	write(1, "--------------------------------------------------\n", 51);
	i = 0;
	write(1, "PATH => ", 8);
	while(path[i])
	{
		write(1, &path[i], 1);
		i++;
	}
	write(1, "\n", 1);
	return 0;
}
