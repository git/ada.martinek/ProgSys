#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

void	my_putnbr(int nb)
{
	char c;

	if (nb < 0)
	{
		if (nb == -2147483648)
		{
			write(1, "-", 1);
			write(1, "2", 1);
			my_putnbr(147483648);
			return ;
		}
		nb = nb * -1;
		write(1, "-", 1);
	}
	if (nb >= 0 && nb < 10)
	{
		c = nb + '0';
		write(1, &c, 1);
	}
	if (nb >= 10)
	{
		my_putnbr(nb / 10);
		my_putnbr(nb % 10);
	}
}

int	main(void)
{
	pid_t	pid;
	int	i;
	int	status;

	i = 0;
	status = 0;
	switch(pid=fork())
	{
		case -1 :
			write(2, "fork foireux\n", 13);
			exit(errno);
		case  0 :
			while (i < 10)
			{
				my_putnbr(i);
				write(1, "\n", 1);
				nanosleep((const struct timespec[]){{0, 500000000L}}, NULL);
				i++;
			}
			system("ps -f");
			exit(2);
		default :
			//waitpid plutot que wait, on sait jamais si un fils caché venait à se présenter... Ça ruine des marriages ce genre d'histoire
			waitpid(pid, &status, 0);
			write(1, "Code de retour du fils : ", 25);
			my_putnbr(status);
			write(1, ".\n", 2);
			write(1, "Fin du processus père de pid ", 30);
			my_putnbr(getpid());
			write(1, ".\n", 3);
	} 
	exit(0);
}
