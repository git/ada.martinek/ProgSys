#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

int		my_atoi(char *str)
{
	int i;
	int posi;
	int num;

	i = 0;
	posi = 1;
	num = 0;
	while (str[i] == 32 || (str[i] >= 9 && str[i] <= 13))
		i++;
	while (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			posi = posi * -1;
		i++;
	}
	while (str[i] >= 48 && str[i] <= 57)
	{
		num = num * 10;
		num = num + (str[i] - 48);
		i++;
	}
	return (num * posi);
}

void	my_putnbr(int nb)
{
	char c;

	if (nb < 0)
	{
		if (nb == -2147483648)
		{
			write(1, "-", 1);
			write(1, "2", 1);
			my_putnbr(147483648);
			return ;
		}
		nb = nb * -1;
		write(1, "-", 1);
	}
	if (nb >= 0 && nb < 10)
	{
		c = nb + '0';
		write(1, &c, 1);
	}
	if (nb >= 10)
	{
		my_putnbr(nb / 10);
		my_putnbr(nb % 10);
	}
}

int	main(int argc, char **argv)
{
	pid_t	pid;
	int	i;
	int	j;
	int	n;

	i = 0;
	j = 0;
	if (argc != 2)
	{
		write(1, "T'es bourré, on veut qu'un seul chiffre qu'on t'dis\n", 53);
		exit(1);
	}
	n = my_atoi(argv[1]);
	while(j < n)
	{
		switch(pid=fork())
		{
			case -1 :
				write(2, "fork foireux\n", 13);
				exit(errno);
			case  0 :
				my_putnbr(getpid());
				while (i < 10)
				{
					my_putnbr(i);
					write(1, "\n", 1);
					nanosleep((const struct timespec[]){{0, 500000000L}}, NULL);
					i++;
				}
				exit(2);
		}
	j++;
	}
        wait(NULL);
        write(1, "Fin du processus père de pid ", 30);
        my_putnbr(getpid());
        write(1, ".\n", 3);
	exit(0);
}
